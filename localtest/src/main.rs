#![deny(clippy::all)]
use std::{rc::Rc, time::Duration};

use dbus::{
    arg::{Array, PropMap, Variant},
    blocking::LocalConnection,
    strings::Interface,
};
use dbus_tree::Factory;

static mut CALL_COUNT: usize = 0;

pub fn main() {
    let conn = LocalConnection::new_session().unwrap();

    conn.request_name("org.mpris.MediaPlayer2.localtest", true, true, false)
        .unwrap();
    let fact = Rc::new(Factory::new_fn::<()>());

    let f = Rc::clone(&fact);
    let f2 = Rc::clone(&fact);

    let tree = f.tree(()).add(
        f.object_path("/org/mpris/MediaPlayer2", ())
            .introspectable()
            .add(
                f.interface("org.mpris.MediaPlayer2", ())
                    .add_m(f.method("Quit", (), |_m| {
                        println!("quit called");
                        Ok(vec![])
                    }))
                    .add_m(f.method("Raise", (), |_m| {
                        println!("raise called");
                        Ok(vec![])
                    })),
            )
            .add(
                f.interface("org.mpris.MediaPlayer2.Player", ())
                    .add_p(
                        f.property::<String, _>("Identity", ())
                            .access(dbus_tree::Access::Read)
                            .on_get(|i, _m| {
                                println!("identity called");
                                i.append("LocalTest");
                                Ok(())
                            }),
                    )
                    .add_m(f.method("PlayPause", (), move |m| {
                        let stage = unsafe {
                            CALL_COUNT += 1;
                            println!("playpause called {} times", CALL_COUNT);
                            CALL_COUNT % 3
                        };
                        let mut props = PropMap::new();

                        let state = match stage {
                            0 => "Paused".to_string(),
                            1 => "Playing".to_string(),
                            2 => "Stopped".to_string(),
                            _ => unreachable!(),
                        };

                        props.insert("PlaybackStatus".to_owned(), Variant(Box::new(state)));

                        let sig = f2
                            .signal("PropertiesChanged", ())
                            .msg(
                                m.path.get_name(),
                                &Interface::new("org.freedesktop.DBus.Properties".to_string())
                                    .unwrap(),
                            )
                            .append3(
                                "org.mpris.MediaPlayer2.Player",
                                props,
                                Array::new(Vec::<String>::new()),
                            );
                        Ok(vec![sig])
                    })),
            ),
    );

    tree.start_receive(&conn);

    loop {
        conn.process(Duration::from_millis(1000)).unwrap();
    }
}
